| NUmber | Stages                 | Start date  |     End Date      | Commit                                                                              |
| ------:| ---------------------- |:-----------:|:-----------------:| ------------------------------------------------------------------------------------|
|      1 | Task Clarification     | 07.04.2023  |    08.04.2023     | Saw course project introduction                                                     |
|      2 | Analysis               | 09.04.2023  |    14.04.2023     | Reviewed the requirements                                                           |
|      3 | Use Cases              | 15.04.2023  |    18.04.2023     | Discussed with my coursemates What the commands are and how they work are discussed |
|      4 | Search for Solutions   | 19.04.2023  |    14.05.2023     | Researched , how to implement controller, service, dao, entity, exception layers    |
|      5 | Software Development   | 15.05.2023  |    06.06.2023     | Hard but satisfying task                                                            |
|      6 | Development Completion | 06.06.2023  |    08.06.2023     | Acquanted with git issues                                                           |
|      7 | Presentation           | 08.06.2023  |    09.06.2023     | Researched short but meaningfull sentences for presentation                         |
