package com.clothes.view;

import com.clothes.model.Product;
import com.clothes.service.ProductServiceImpl;

import java.util.List;
import java.util.Scanner;

public class ConsoleView {
    private final ProductServiceImpl productServiceImpl;
    private final Scanner scanner;

    public ConsoleView(ProductServiceImpl productServiceImpl) {
        this.productServiceImpl = productServiceImpl;
        this.scanner = new Scanner(System.in);
    }

    public void displayMenu() {
        System.out.println("===  Clothes and Footwear's Search System  ===");
        System.out.println("1. Search products by name");
        System.out.println("2. Search products by category");
        System.out.println("3. Search products by price range");
        System.out.println("4. Search products by quantity range");
        System.out.println("5. Search products by made in");
        System.out.println("6. Search products by creation date");
        System.out.println("7. Search products by material");
        System.out.println("8. Search products by used for");
        System.out.println("9. Search products by season");
        System.out.println("10. List all products");
        System.out.println("11. Exit");
    }

    public void run() {
        System.out.println("Welcome to the Warehouse Search System!");

        while (true) {
            displayMenu();
            System.out.print("Enter your choice: ");
            int choice = scanner.nextInt();
            scanner.nextLine();

            switch (choice) {
                case 1 -> searchProductsByName();
                case 2 -> searchProductsByCategory();
                case 3 -> searchProductsByPriceRange();
                case 4 -> searchProductsByQuantityRange();
                case 5 -> searchProductsByMadeIn();
                case 6 -> searchProductsByCreationDate();
                case 7 -> searchProductsByMaterial();
                case 8 -> searchProductsByUsedFor();
                case 9 -> searchProductsBySeason();
                case 10 -> listAllProducts();
                case 11 -> {
                    System.out.println("Thank you for using the Warehouse Search System. Goodbye!");
                    return;
                }
                default -> System.out.println("Invalid choice. Please try again.");
            }

            System.out.println();
        }
    }

    private void searchProductsByName() {
        System.out.print("Enter the product name: ");
        String name = scanner.nextLine();

        List<Product> products = productServiceImpl.searchProductsByName(name);

        if (products.isEmpty()) {
            System.out.println("No products found with the name: " + name);
        } else {
            displayProductList(products);
        }
    }

    private void searchProductsByCategory() {
        System.out.print("Enter the product category: ");
        String category = scanner.nextLine();

        List<Product> products = productServiceImpl.searchProductsByCategory(category);

        if (products.isEmpty()) {
            System.out.println("No products found in the category: " + category);
        } else {
            displayProductList(products);
        }
    }

    private void searchProductsByPriceRange() {
        System.out.print("Enter the minimum price: ");
        double minPrice = scanner.nextDouble();
        System.out.print("Enter the maximum price: ");
        double maxPrice = scanner.nextDouble();

        List<Product> products = productServiceImpl.searchProductsByPriceRange(minPrice, maxPrice);

        if (products.isEmpty()) {
            System.out.println("No products found within the price range: " + minPrice + " - " + maxPrice);
        } else {
            displayProductList(products);
        }
    }

    private void searchProductsByQuantityRange() {
        System.out.print("Enter the minimum quantity: ");
        int minQuantity = scanner.nextInt();
        System.out.print("Enter the maximum quantity: ");
        int maxQuantity = scanner.nextInt();

        List<Product> products = productServiceImpl.searchProductsByQuantityRange(minQuantity, maxQuantity);

        if (products.isEmpty()) {
            System.out.println("No products found within the quantity range: " + minQuantity + " - " + maxQuantity);
        } else {
            displayProductList(products);
        }
    }

    private void searchProductsByMadeIn() {
        System.out.print("Enter the 'Made In' attribute: ");
        String madeIn = scanner.nextLine().trim();

        List<Product> products = productServiceImpl.searchProductsByMadeIn(madeIn);

        if (products.isEmpty()) {
            System.out.println("No products found with the 'Made In' attribute: " + madeIn);
        } else {
            displayProductList(products);
        }
    }


    private void searchProductsByCreationDate() {
        System.out.print("Enter the 'Creation Date' attribute: ");
        String creationDate = scanner.nextLine();

        List<Product> products = productServiceImpl.searchProductsByCreationDate(creationDate);

        if (products.isEmpty()) {
            System.out.println("No products found with the 'Creation Date' attribute: " + creationDate);
        } else {
            displayProductList(products);
        }
    }

    private void searchProductsByMaterial() {
        System.out.print("Enter the 'Material' attribute: ");
        String material = scanner.nextLine();

        List<Product> products = productServiceImpl.searchProductsByMaterial(material);

        if (products.isEmpty()) {
            System.out.println("No products found with the 'Material' attribute: " + material);
        } else {
            displayProductList(products);
        }
    }

    private void searchProductsByUsedFor() {
        System.out.print("Enter the 'Used For' attribute: ");
        String usedFor = scanner.nextLine();

        List<Product> products = productServiceImpl.searchProductsByUsedFor(usedFor);

        if (products.isEmpty()) {
            System.out.println("No products found with the 'Used For' attribute: " + usedFor);
        } else {
            displayProductList(products);
        }
    }

    private void searchProductsBySeason() {
        System.out.print("Enter the 'Season' attribute: ");
        String season = scanner.nextLine();

        List<Product> products = productServiceImpl.searchProductsBySeason(season);

        if (products.isEmpty()) {
            System.out.println("No products found with the 'Season' attribute: " + season);
        } else {
            displayProductList(products);
        }
    }

    private void listAllProducts() {
        List<Product> products = productServiceImpl.getAllProducts();

        if (products.isEmpty()) {
            System.out.println("No products available in the inventory.");
        } else {
            displayProductList(products);
        }
    }

    private void displayProductList(List<Product> products) {
        System.out.println("=== Product List ===");
        for (Product product : products) {
            System.out.println(product);
        }
    }
}
