package com.clothes.model;

import java.util.Objects;

public class Product {
    private final int id;
    private final String productName;
    private final String type;
    private final int quantity;
    private final int productId;
    private final String madeIn;
    private final String creationDate;
    private final String material;
    private final String usedFor;
    private final String season;
    private final double price;

    public Product(int id, String productName, String type, int quantity, int productId, String madeIn, String creationDate, String material, String usedFor, String season, double price) {
        this.id = id;
        this.productName = productName;
        this.type = type;
        this.quantity = quantity;
        this.productId = productId;
        this.madeIn = madeIn;
        this.creationDate = creationDate;
        this.material = material;
        this.usedFor = usedFor;
        this.season = season;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public String getProductName() {
        return productName;
    }

    public String getType() {
        return type;
    }

    public int getQuantity() {
        return quantity;
    }

    public int getProductId() {
        return productId;
    }

    public String getMadeIn() {
        return madeIn;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public String getMaterial() {
        return material;
    }

    public String getUsedFor() {
        return usedFor;
    }

    public String getSeason() {
        return season;
    }

    public double getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", productName='" + productName + '\'' +
                ", type='" + type + '\'' +
                ", quantity=" + quantity +
                ", productId=" + productId +
                ", madeIn='" + madeIn + '\'' +
                ", creationDate='" + creationDate + '\'' +
                ", material='" + material + '\'' +
                ", usedFor='" + usedFor + '\'' +
                ", season='" + season + '\'' +
                ", price=" + price +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return id == product.id && quantity == product.quantity && productId == product.productId && Double.compare(product.price, price) == 0 && Objects.equals(productName, product.productName) && Objects.equals(type, product.type) && Objects.equals(madeIn, product.madeIn) && Objects.equals(creationDate, product.creationDate) && Objects.equals(material, product.material) && Objects.equals(usedFor, product.usedFor) && Objects.equals(season, product.season);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, productName, type, quantity, productId, madeIn, creationDate, material, usedFor, season, price);
    }
}
