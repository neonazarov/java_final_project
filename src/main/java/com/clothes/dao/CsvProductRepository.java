package com.clothes.dao;

import com.clothes.model.Product;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class CsvProductRepository implements ProductRepository {
    private static final String DATA_FILE_PATH = "src/resources/data.csv";

    public CsvProductRepository() {
    }

    @Override
    public List<Product> getAllProducts() {
        List<Product> products = new ArrayList<>();

        try (BufferedReader reader = new BufferedReader(new FileReader(DATA_FILE_PATH))) {
            String line;
            reader.readLine();

            while ((line = reader.readLine()) != null) {
                String[] data = line.split(",");
                int id = Integer.parseInt(data[0].trim());
                String productName = data[1].trim();
                String type = data[2].trim();
                int quantity = Integer.parseInt(data[3].trim());
                int productId = Integer.parseInt(data[4].trim());
                String madeIn = data[5].trim();
                String creationDate = data[6].trim();
                String material = data[7].trim();
                String usedFor = data[8].trim();
                String season = data[9].trim();
                double price = Double.parseDouble(data[10].replaceAll("[^0-9.]", ""));

                Product product = new Product(id, productName, type, quantity, productId, madeIn, creationDate, material, usedFor, season, price);
                products.add(product);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return products;
    }

    @Override
    public List<Product> searchProductsByName(String name) {
        List<Product> products = getAllProducts();
        return products.stream()
                .filter(product -> product.getProductName().equalsIgnoreCase(name))
                .collect(Collectors.toList());
    }

    @Override
    public List<Product> searchProductsByCategory(String category) {
        List<Product> products = getAllProducts();
        return products.stream()
                .filter(product -> product.getType().equalsIgnoreCase(category))
                .collect(Collectors.toList());
    }

    @Override
    public List<Product> searchProductsByPriceRange(double minPrice, double maxPrice) {
        List<Product> products = getAllProducts();
        return products.stream()
                .filter(product -> product.getPrice() >= minPrice && product.getPrice() <= maxPrice)
                .collect(Collectors.toList());
    }

    @Override
    public List<Product> searchProductsByQuantityRange(int minQuantity, int maxQuantity) {
        List<Product> products = getAllProducts();
        return products.stream()
                .filter(product -> product.getQuantity() >= minQuantity && product.getQuantity() <= maxQuantity)
                .collect(Collectors.toList());
    }

    @Override
    public List<Product> searchProductsByMadeIn(String madeIn) {
        List<Product> products = getAllProducts();
        return products.stream()
                .filter(product -> product.getMadeIn().equalsIgnoreCase(madeIn))
                .collect(Collectors.toList());
    }

    @Override
    public List<Product> searchProductsByCreationDate(String creationDate) {
        List<Product> products = getAllProducts();
        return products.stream()
                .filter(product -> product.getCreationDate().equalsIgnoreCase(creationDate))
                .collect(Collectors.toList());
    }

    @Override
    public List<Product> searchProductsByMaterial(String material) {
        List<Product> products = getAllProducts();
        return products.stream()
                .filter(product -> product.getMaterial().equalsIgnoreCase(material))
                .collect(Collectors.toList());
    }

    @Override
    public List<Product> searchProductsByUsedFor(String usedFor) {
        List<Product> products = getAllProducts();
        return products.stream()
                .filter(product -> product.getUsedFor().equalsIgnoreCase(usedFor))
                .collect(Collectors.toList());
    }

    @Override
    public List<Product> searchProductsBySeason(String season) {
        List<Product> products = getAllProducts();
        return products.stream()
                .filter(product -> product.getSeason().equalsIgnoreCase(season))
                .collect(Collectors.toList());
    }


    @Override
    public List<Product> sortProductsByField(String field) {
        List<Product> products = getAllProducts();

        switch (field) {
            case "id" -> products.sort(Comparator.comparing(Product::getId));
            case "name" -> products.sort(Comparator.comparing(Product::getProductName));
            case "category" -> products.sort(Comparator.comparing(Product::getType));
            case "price" -> products.sort(Comparator.comparing(Product::getPrice));
            case "quantity" -> products.sort(Comparator.comparing(Product::getQuantity));
            case "madeIn" -> products.sort(Comparator.comparing(Product::getMadeIn));
            case "creationDate" -> products.sort(Comparator.comparing(Product::getCreationDate));
            case "material" -> products.sort(Comparator.comparing(Product::getMaterial));
            case "usedFor" -> products.sort(Comparator.comparing(Product::getUsedFor));
            case "season" -> products.sort(Comparator.comparing(Product::getSeason));
            default -> {
            }

        }

        return products;
    }
}
