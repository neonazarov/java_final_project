package com.clothes.controller;

public interface AdminController {
    ResponseEntity<?> addRecord(RecordDto recordDto);
    ResponseEntity<?> deleteRecord(Long id);
    ResponseEntity<?> addCategory(CategoryDto categoryDto);
    // Other administrative endpoints
}
