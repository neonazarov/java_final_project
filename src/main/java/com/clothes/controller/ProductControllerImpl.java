package com.clothes.controller;

import com.clothes.view.ConsoleView;

public class ProductControllerImpl implements ProductController {
    private final ConsoleView consoleView;

    public ProductControllerImpl(ConsoleView consoleView) {
        this.consoleView = consoleView;
    }

    public void startApplication() {
        displayApplicationInformation();
        consoleView.run();
    }

    public void displayApplicationInformation() {
        System.out.println("=== Clothes and Footwear's Search System ===");
        System.out.println("Version: 1.0");
        System.out.println("Developer:https://github.com/neonazarov");
        System.out.println("Email:nurmuhammad_nazarov@student.itpu.uz");
        System.out.println();
    }
}
