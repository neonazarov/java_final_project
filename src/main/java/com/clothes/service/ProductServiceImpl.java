package com.clothes.service;

import com.clothes.model.Product;
import com.clothes.dao.ProductRepository;

import java.util.List;

public class ProductServiceImpl implements ProductService {
    private final ProductRepository productRepository;

    public ProductServiceImpl(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public List<Product> getAllProducts() {
        return productRepository.getAllProducts();
    }

    public List<Product> searchProductsByName(String name) {
        return productRepository.searchProductsByName(name);
    }

    public List<Product> searchProductsByCategory(String category) {
        return productRepository.searchProductsByCategory(category);
    }

    public List<Product> searchProductsByPriceRange(double minPrice, double maxPrice) {
        return productRepository.searchProductsByPriceRange(minPrice, maxPrice);
    }

    public List<Product> searchProductsByQuantityRange(int minQuantity, int maxQuantity) {
        return productRepository.searchProductsByQuantityRange(minQuantity, maxQuantity);
    }

    public List<Product> searchProductsByMadeIn(String madeIn) {
        return productRepository.searchProductsByMadeIn(madeIn);
    }

    public List<Product> searchProductsByCreationDate(String creationDate) {
        return productRepository.searchProductsByCreationDate(creationDate);
    }

    public List<Product> searchProductsByMaterial(String material) {
        return productRepository.searchProductsByMaterial(material);
    }

    public List<Product> searchProductsByUsedFor(String usedFor) {
        return productRepository.searchProductsByUsedFor(usedFor);
    }

    public List<Product> searchProductsBySeason(String season) {
        return productRepository.searchProductsBySeason(season);
    }
}
