package com.clothes.service;

import com.clothes.model.Product;

import java.util.List;

public interface ProductService {
    List<Product> getAllProducts();

    List<Product> searchProductsByName(String name);

    List<Product> searchProductsByCategory(String category);

    List<Product> searchProductsByPriceRange(double minPrice, double maxPrice);

    List<Product> searchProductsByQuantityRange(int minQuantity, int maxQuantity);

    List<Product> searchProductsByMadeIn(String madeIn);

    List<Product> searchProductsByCreationDate(String creationDate);

    List<Product> searchProductsByMaterial(String material);

    List<Product> searchProductsByUsedFor(String usedFor);

    List<Product> searchProductsBySeason(String season);
}
