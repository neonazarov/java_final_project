package com.clothes.service;

import com.clothes.model.Product;

import java.util.List;

public interface AdminService {
    void addRecord(Record record);
    void deleteRecord(Long id);
    void addCategory(Category category);
    // Additional administrative operations
}

