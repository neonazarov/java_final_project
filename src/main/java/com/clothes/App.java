package com.clothes;

import com.clothes.controller.ProductControllerImpl;
import com.clothes.dao.ProductRepository;
import com.clothes.dao.CsvProductRepository;
import com.clothes.service.ProductServiceImpl;
import com.clothes.view.ConsoleView;

public class App {
    public static void main(String[] args) {
        ProductRepository repository = new CsvProductRepository();
        ProductServiceImpl service = new ProductServiceImpl(repository);
        ConsoleView view = new ConsoleView(service);
        ProductControllerImpl controller = new ProductControllerImpl(view);
        controller.startApplication();
    }
}
