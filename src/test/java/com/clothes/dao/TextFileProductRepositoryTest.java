package com.clothes.dao;

import com.clothes.model.Product;

import java.util.List;

public class TextFileProductRepositoryTest {
    public static void main(String[] args) {
        // Create the repository
        CsvProductRepository productRepository = new CsvProductRepository();

        // Test the getAllProducts() method
        List<Product> allProducts = productRepository.getAllProducts();
        System.out.println("All Products:");
        for (Product product : allProducts) {
            System.out.println(product);
        }

        // Test the searchProductsByName() method
        String searchName = "Shirt";
        List<Product> productsByName = productRepository.searchProductsByName(searchName);
        System.out.println("\nProducts with Name '" + searchName + "':");
        for (Product product : productsByName) {
            System.out.println(product);
        }

        // Test the searchProductsByCategory() method
        String searchCategory = "Footwear";
        List<Product> productsByCategory = productRepository.searchProductsByCategory(searchCategory);
        System.out.println("\nProducts in Category '" + searchCategory + "':");
        for (Product product : productsByCategory) {
            System.out.println(product);
        }

        // Test the searchProductsByPriceRange() method
        double minPrice = 50.0;
        double maxPrice = 100.0;
        List<Product> productsByPriceRange = productRepository.searchProductsByPriceRange(minPrice, maxPrice);
        System.out.println("\nProducts within Price Range $" + minPrice + " - $" + maxPrice + ":");
        for (Product product : productsByPriceRange) {
            System.out.println(product);
        }

        // Test the searchProductsByQuantityRange() method
        int minQuantity = 10;
        int maxQuantity = 50;
        List<Product> productsByQuantityRange = productRepository.searchProductsByQuantityRange(minQuantity, maxQuantity);
        System.out.println("\nProducts within Quantity Range " + minQuantity + " - " + maxQuantity + ":");
        for (Product product : productsByQuantityRange) {
            System.out.println(product);
        }

        // Add more tests for other methods as needed

    }
}
