package com.clothes.controller;

import com.clothes.view.ConsoleView;
import com.clothes.service.ProductServiceImpl;
import com.clothes.dao.CsvProductRepository;

public class ProductControllerTest {
    public static void main(String[] args) {
        // Create the dependencies
        CsvProductRepository productRepository = new CsvProductRepository();
        ProductServiceImpl productServiceImpl = new ProductServiceImpl(productRepository);
        ConsoleView consoleView = new ConsoleView(productServiceImpl);

        // Create the controller
        ProductControllerImpl productControllerImpl = new ProductControllerImpl(consoleView);

        // Start the application
        productControllerImpl.startApplication();
    }
}
