package com.clothes.service;

import com.clothes.model.Product;
import com.clothes.dao.ProductRepository;
import com.clothes.dao.CsvProductRepository;

import java.util.List;

public class ProductServiceTest {
    public static void main(String[] args) {
        // Create the repository
        ProductRepository productRepository = new CsvProductRepository();

        // Create the service
        ProductServiceImpl productServiceImpl = new ProductServiceImpl(productRepository);

        // Test the getAllProducts() method
        List<Product> allProducts = productServiceImpl.getAllProducts();
        System.out.println("All Products:");
        for (Product product : allProducts) {
            System.out.println(product);
        }

        // Test the searchProductsByName() method
        String searchName = "Shirt";
        List<Product> productsByName = productServiceImpl.searchProductsByName(searchName);
        System.out.println("\nProducts with Name '" + searchName + "':");
        for (Product product : productsByName) {
            System.out.println(product);
        }

        // Test the searchProductsByCategory() method
        String searchCategory = "Footwear";
        List<Product> productsByCategory = productServiceImpl.searchProductsByCategory(searchCategory);
        System.out.println("\nProducts in Category '" + searchCategory + "':");
        for (Product product : productsByCategory) {
            System.out.println(product);
        }

        // Test the searchProductsByPriceRange() method
        double minPrice = 50.0;
        double maxPrice = 100.0;
        List<Product> productsByPriceRange = productServiceImpl.searchProductsByPriceRange(minPrice, maxPrice);
        System.out.println("\nProducts within Price Range $" + minPrice + " - $" + maxPrice + ":");
        for (Product product : productsByPriceRange) {
            System.out.println(product);
        }

        // Test the searchProductsByQuantityRange() method
        int minQuantity = 10;
        int maxQuantity = 50;
        List<Product> productsByQuantityRange = productServiceImpl.searchProductsByQuantityRange(minQuantity, maxQuantity);
        System.out.println("\nProducts within Quantity Range " + minQuantity + " - " + maxQuantity + ":");
        for (Product product : productsByQuantityRange) {
            System.out.println(product);
        }

        // Add more tests for other methods as needed

    }
}
